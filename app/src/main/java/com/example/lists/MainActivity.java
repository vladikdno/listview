package com.example.lists;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String users[] = {
                "Me", "Me", "Me", "Me", "Me", "Me", "You", "He", "She", "It"
        };

        ListView listView = findViewById(R.id.list_view);
        MyAdapter adapter = new MyAdapter(this, users);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {//отслеживаем нажатия на
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {//перед этим обязательно пишем массив final
                Toast.makeText(MainActivity.this, users[position], Toast.LENGTH_SHORT).show();//и выводим текст
                // в котором показываем текст на который нажали
            }
        });
    }
}
