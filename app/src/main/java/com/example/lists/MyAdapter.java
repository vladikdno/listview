package com.example.lists;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyAdapter extends ArrayAdapter<String> {
    public MyAdapter(Context context, String[] objects) {
        super(context, R.layout.item_user, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());//создаем inflanter
        View rowView = inflater.inflate(R.layout.item_user, parent, false);//ищем разметку в xml

        TextView textView = rowView.findViewById(R.id.textView);//
        textView.setText(getItem(position));

        return rowView;
    }
}
